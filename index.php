<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercices PDO</title>
</head>

<body>

    <?php

    $bdd = new PDO('mysql:host=localhost;dbname=colyseum;charset=utf8', 'root', 'root');

    try {
        $bdd = new PDO('mysql:host=localhost;dbname=colyseum;charset=utf8', 'root', 'root');
    } catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
    }

    $listClients = $bdd->query('SELECT * FROM clients'); ?>

    <strong>Liste des clients :</strong>

    <!-- Ex 1 - Affiche les prénoms/noms de la table Clients -->
    <?php
    while ($donnees = $listClients->fetch()) {
        ?> <p> <?php echo $donnees['firstName']; ?>
            <?php echo $donnees['lastName']; ?><br /></p>
    <?php
    }
    $listClients->closeCursor(); // Termine le traitement de la requête

    // Ex 2 - Liste des types de spectacles 

    $listTypeShows = $bdd->query('SELECT * FROM showTypes'); ?>

    <strong>Liste des types spectacles :</strong>

    <p> <?php while ($donnees = $listTypeShows->fetch()) {
            ?> <p> <?php echo $donnees['type']; ?> </p>
        <?php
        }
        $listTypeShows->CloseCursor();

        // Ex 3 - Liste des 20 premiers clients 

        $list20Clients = $bdd->query('SELECT * FROM clients LIMIT 0, 20'); ?>
        <strong>Liste des 20 premiers clients :</strong>
        <p> <?php while ($donnees = $list20Clients->fetch()) {
                ?><p> <?php echo $donnees['firstName']; ?>
                    <?php echo $donnees['lastName']; ?></p>
            <?php
            }
            $list20Clients->CloseCursor();

            // Ex 4 - Afficher les clients possédant une carte   

            $listClientsWithCards = $bdd->query('SELECT * FROM clients WHERE card=1'); ?>
            <strong>Liste des clients avec cartes :</strong>
            <p><?php while ($donnees = $listClientsWithCards->fetch()) {
                    ?><p><?php echo $donnees['firstName'];?>
                        <?php echo $donnees['lastName']; ?></p>
                <?php
                }
                $listClientsWithCards->CloseCursor();
              
// <!-- Ex 5 - Afficher uniquement le nom et le prénom de tous les clients dont le nom commence par la lettre "M".
// Les afficher comme ceci : 
// Nom : *Nom du client*
// Prénom : *Prénom du client*
// Trier les noms par ordre alphabétique.


// Ex 6 - Afficher le titre de tous les spectacles ainsi que l'artiste, la date et l'heure. Trier les titres par ordre alphabétique. Afficher les résultat comme ceci : *Spectacle* par *artiste*, le *date* à *heure*.

// Exercice 7 - Afficher tous les clients comme ceci :
// Nom : *Nom de famille du client*
// Prénom : *Prénom du client*
// Date de naissance : *Date de naissance du client*
// Carte de fidélité : *Oui (Si le client en possède une) ou Non (s'il n'en possède pas)*
// Numéro de carte : *Numéro de la carte fidélité du client s'il en possède une.*

?>          

</body>

</html>